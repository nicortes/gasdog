﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para cambiar escenas
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame() {

        Scene CurrentScene = SceneManager.GetActiveScene();

        int CurrentSceneIndex = CurrentScene.buildIndex;

        int NextSceneIndex = CurrentSceneIndex + 1;

        SceneManager.LoadScene(NextSceneIndex);
    }


    public void QuitGame() {

        print("HAS SALIDO DEL JUEGO");

        Application.Quit();

    }

}
