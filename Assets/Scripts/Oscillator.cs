﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]// Hace que sólo pueda haber 1 script de este tipo en el GameObject donde se ponga como componente
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector;// Nueva posición a la que va a llegar
    [SerializeField] float period;// El periodo es el tiempo que tarda en hacerse 1 ciclo

    //[Range(0, 1)]// Para restringir entre 0 y 1 en el Inspector
    float movementFactor;// Proporción de la nueva posición, o qué tan rápido llegará a ella

    Vector3 startingPosition;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //print(Mathf.Epsilon);// Mathf.Epsilon es el float más pequeño que existe en Unity. Se compara con él, porque a veces comparar con 0 puede fallar (diferencias microscópicas entre un float 0 y otro float 0)
        if (period <= Mathf.Epsilon) {
            return; // Si fuera a divir por cero, se sale y no hace nada
        }

        float cycles = Time.time / period;// cantidad de ciclos que se han completado desde que inició el juego

        const float tau = Mathf.PI * 2;// Tau = 2 pi
        float rawSinWave = Mathf.Sin(cycles * tau);// Como es un seno, arroja valores entre -1 y 1

        movementFactor = rawSinWave / 2f + 0.5f;// Se divide por 2 y se suma 0.5 para que movementFactor vaya de 0 a 1;

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
    }
}
