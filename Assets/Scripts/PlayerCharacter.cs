﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    GameObject ned;
    GameObject R2D2;

    Vector3 characterPosition;
    Quaternion characterRotation;// No me deja usar Vector3, me pide usar Quaternion

    static public Vector3 initialPosition;
    static public Quaternion initialRotation;

    public static bool playerIsNed;

    AudioSource audioSource;

    [SerializeField] AudioClip r2d2Sound;
    [SerializeField] AudioClip dogSound;

    // Start is called before the first frame update
    void Start()
    {
        ned = GameObject.Find("Ned");
        R2D2 = GameObject.Find("R2-D2");

        initialPosition = ned.transform.position;
        initialRotation = ned.transform.rotation;

        R2D2.SetActive(false);

        playerIsNed = true;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        CharacterSwitch();
    }
    
    private void CharacterSwitch() {

        if (Input.GetKeyDown(KeyCode.J) && Ned.state == Ned.State.Alive) {
            ChangeCharacter();
        }
    }

    private void ChangeCharacter() {

        if (!Ned.GameIsPaused) {
            if (ned.activeInHierarchy) {
                ChangeToR2D2();

            } else if (R2D2.activeInHierarchy) {
                ChangeToNed();
            }
        }
    }

    private void ChangeToNed() {
        R2D2.GetComponent<Rigidbody>().velocity = Vector3.zero;

        characterPosition = R2D2.transform.position;
        characterRotation = R2D2.transform.rotation;

        R2D2.SetActive(false);
        ned.SetActive(true);

        ned.transform.position = characterPosition;
        ned.transform.rotation = characterRotation;

        audioSource.Stop();
        audioSource.PlayOneShot(dogSound);

        playerIsNed = true;
    }

    private void ChangeToR2D2() {
        ned.GetComponent<Rigidbody>().velocity = Vector3.zero;

        characterPosition = ned.transform.position;
        characterRotation = ned.transform.rotation;

        ned.SetActive(false);
        R2D2.SetActive(true);

        R2D2.transform.position = characterPosition;
        R2D2.transform.rotation = characterRotation;

        audioSource.Stop();
        audioSource.PlayOneShot(r2d2Sound);

        playerIsNed = false;
    }
}
