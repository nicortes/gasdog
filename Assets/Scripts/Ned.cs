﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ned : MonoBehaviour
{
    // rcs = Reaction Control System
    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 20f;
    [SerializeField] float levelLoadDelay = 1f;

    [SerializeField] AudioClip bubblesSound;
    [SerializeField] AudioClip victorySound;
    [SerializeField] AudioClip deathSound;
    [SerializeField] AudioClip r2d2DeathSound;

    [SerializeField] ParticleSystem bubblesParticles;
    [SerializeField] ParticleSystem victoryParticles;
    [SerializeField] ParticleSystem deathParticles;

    Rigidbody rigidBody;
    AudioSource audioSource;

    /* Otra forma de usar múltiples audios
    AudioSource[] audioSources;
    AudioSource bubblesSound;
    AudioSource victorySound;
    */

    public GameObject VictoryMenuUI;
    public GameObject PauseButtonUI;

    public enum State {Alive, Dying, Transcending}
    public static State state;

    public static bool GameIsPaused;

    Scene CurrentScene;
    int CurrentSceneIndex;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        /* Otra forma de usar múltiples audios
        audioSources = GetComponents<AudioSource>();
        bubblesSound = audioSources[0];
        victorySound = audioSources[1];
        */
        //initialPosition = transform.position;
        //initialRotation = transform.rotation;
        GameIsPaused = false;
        Time.timeScale = 1f;
        CurrentScene = SceneManager.GetActiveScene();
        CurrentSceneIndex = CurrentScene.buildIndex;
        state = State.Alive;
}

    // Update is called once per frame
    void Update()
    {
        //print("estado= "+state);
        if (state == State.Alive) { // Cuando se muere o va a cambiar de nivel, el estado cambia y por eso ya no se puede mover al personaje
            RespondToThrustInput();
            Rotate();
            Commands();
        }
        if (state == State.Dying) {
            bubblesParticles.Stop();
        }
    }

    private void RespondToThrustInput() {
        // GetKey es para detectar la tecla mientras se mantiene presionada
        if (Input.GetKey(KeyCode.W) && !GameIsPaused) {
            ApplyThrust();

        } else {  
            /* Otra forma de usar múltiples audios
                bubblesSound.Stop();
            */
            audioSource.Stop();
            bubblesParticles.Stop();
        }
    }

    private void ApplyThrust() {

        float thrustThisFrame = mainThrust * Time.deltaTime;

        rigidBody.AddRelativeForce(Vector3.up * thrustThisFrame);

        //rigidBody.AddRelativeForce(Vector3.up * mainThrust);// Si se activa esta línea, el mainThrust del Inspector debe ser 20

        if (!audioSource.isPlaying) {
            audioSource.PlayOneShot(bubblesSound);
        }
        /* Otra forma de usar múltiples audios
        if (!bubblesSound.isPlaying) {
            bubblesSound.Play();
        }
        */
        if (!bubblesParticles.isEmitting) {// Muy importante. Como bubblesParticles tiene Looping activado en el inspector, tiene que estar este if para que funcionen las partículas de las burbujas
            bubblesParticles.Play();
        }
    }

    private void Rotate() {

        rigidBody.freezeRotation = true; // Se toma control manual de la rotación

        float rotationThisFrame = rcsThrust * Time.deltaTime;

        Vector3 rotationSpeed = Vector3.forward * rotationThisFrame;

        if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D)) {
            transform.Rotate(rotationSpeed);
        }

        if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A)) {
            transform.Rotate(-rotationSpeed);
        }

        rigidBody.freezeRotation = false; // Las físicas vuelven a tomar el control de la rotación
    }

    private void Commands() {
        
        if (Input.GetKeyDown(KeyCode.R) && !GameIsPaused) {
            Reset();
        }
    }

    private void Reset() {
        state = State.Alive;
        audioSource.Stop();
        deathParticles.Stop();
        /*
        bubblesSound.Stop();
        */
        transform.position = PlayerCharacter.initialPosition;
        transform.rotation = PlayerCharacter.initialRotation;
        rigidBody.velocity = Vector3.zero;
        // Le pongo angularVelocity también, por si acaso
        rigidBody.angularVelocity = Vector3.zero;
    }

    public void FreezeInPlace() {

        GameIsPaused = true;

        // Para ralentizar el tiempo o detenerlo si está en 0.
        Time.timeScale = 0f;

        //rigidBody = GetComponent<Rigidbody>();
        //rigidBody.velocity = Vector3.zero;
        //rigidBody.angularVelocity = Vector3.zero;
        //rigidBody.isKinematic = true;
        //rigidBody.constraints = RigidbodyConstraints.FreezeAll;
    }

    public void UnFreezeInPlace() {

        GameIsPaused = false;

        // Para ralentizar el tiempo o detenerlo si está en 0.
        // En este caso no me funciona para detener el juego.
        Time.timeScale = 1f;
    }

    // Se activa cada vez que hay una colisión
    void OnCollisionEnter(Collision collision) {

        if (state != State.Alive) {
            return;
        }

        switch (collision.gameObject.tag) {
            case "Friendly":
                print("OK");
                break;
            case "Deadly":
                StartDeathSequence();
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            case "Fuel":
                print("GOT FUEL");
                break;
            default:
                print("DEFAULT");
                break;
        }
    }
    
    void StartDeathSequence() {
        state = State.Dying;
        deathParticles.Play();
        audioSource.Stop();
        if (PlayerCharacter.playerIsNed){
            audioSource.PlayOneShot(deathSound);
        } else {
            audioSource.PlayOneShot(r2d2DeathSound);
        }
        // Invoke sirve para llamar un método después de ciertos segundos
        Invoke("Reset", levelLoadDelay);
    }
    
    void StartSuccessSequence() {
        if (state != State.Dying) {
            state = State.Transcending;
            victoryParticles.Play();
            audioSource.Stop();
            audioSource.PlayOneShot(victorySound);
            if (CurrentSceneIndex == 3) {
                EndOfGame();
            } else {
                // Invoke sirve para llamar un método después de ciertos segundos
                Invoke("LoadNextScene", levelLoadDelay);
            }
        }
    }

    void EndOfGame() {
        //FreezeInPlace();
        BackgroundMusic.Audio.Stop();
        PauseButtonUI.SetActive(false);
        VictoryMenuUI.SetActive(true);
    }

    void LoadNextScene() {
        int NextSceneIndex;
        NextSceneIndex = CurrentSceneIndex + 1;
        SceneManager.LoadScene(NextSceneIndex);
    }
}
