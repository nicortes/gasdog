﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para cambiar escenas
using UnityEngine.SceneManagement;

public class LandingPad : MonoBehaviour
{

    public static bool GameIsPaused = false;

    public GameObject VictoryMenuUI;
    public GameObject PauseButtonUI;

    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Se activa cada vez que hay una colisión
    void OnCollisionEnter(Collision collision) {
        /*
        if (collision.gameObject.tag == "Player") {
            Scene CurrentScene = SceneManager.GetActiveScene();
            int CurrentSceneIndex = CurrentScene.buildIndex;
            //print(CurrentSceneIndex);

            if (CurrentSceneIndex == 3) {

                Time.timeScale = 0f;
                BackgroundMusic.Audio.Stop();

                if (!audioSource.isPlaying) {
                    audioSource.Play();
                }
                
                PauseButtonUI.SetActive(false);
                VictoryMenuUI.SetActive(true);
                GameIsPaused = true;

            } else {
                
                int NextSceneIndex = CurrentSceneIndex + 1;
                SceneManager.LoadScene(NextSceneIndex);
            }
        }
    */
    }
}
