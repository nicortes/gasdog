﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;

    public GameObject PauseMenuUI;
    public GameObject PauseButtonUI;

    string MainMenuSceneName = "MainMenu";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PauseController() {
        if (GameIsPaused) {
            Resume();
        } else {
            Pause();
        }
    }

    public void Resume() {

        // Reanudar la música de fondo
        //BackgroundMusic.Instance.gameObject.GetComponent<AudioSource>().Play();
        BackgroundMusic.Audio.Play();

        PauseMenuUI.SetActive(false);
        
        // Un timeScale de 1 es velocidad normal.
        // En este caso no me funciona para detener el juego.
        Time.timeScale = 1f;
        
        GameIsPaused = false;
        PauseButtonUI.SetActive(true);
    }

    public  void Pause() {

        // Pausar la música de fondo
        //BackgroundMusic.Instance.gameObject.GetComponent<AudioSource>().Pause();
        BackgroundMusic.Audio.Pause();
        //BackgroundMusic.PauseMusic();

        PauseMenuUI.SetActive(true);
        
        GameIsPaused = true;
    }

    public void LoadMenu() {

        Time.timeScale = 1f;
        BackgroundMusic.Audio.Play();
        //VictoryMenuUI.SetActive(true);
        GameIsPaused = false;
        LandingPad.GameIsPaused = false;
        Ned.GameIsPaused = false;
        SceneManager.LoadScene(MainMenuSceneName);
    }

}