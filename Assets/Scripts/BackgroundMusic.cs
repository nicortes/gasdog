﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{

    private static BackgroundMusic instance = null;

    private static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        //instance.gameObject.GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Awake es llamada después de que todos los objetos hayan sido inicializados.
    private void Awake() {
        
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
            audioSource = instance.gameObject.GetComponent<AudioSource>();
        }
        
        DontDestroyOnLoad(this.gameObject);
    }

    public static BackgroundMusic Instance {
        get {
            return instance;
        }
    }

    public static AudioSource Audio {
        get {
            return audioSource;
        }
    }

    public static void PauseMusic() {
        BackgroundMusic.Instance.gameObject.GetComponent<AudioSource>().Pause();
    }
}
